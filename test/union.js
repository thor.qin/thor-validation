/* eslint-disable @typescript-eslint/no-var-requires */
const { Schema, object, desc, need, string, mismatch, prop, union } = require('../dist');

let type1 = object(
	prop('id', desc('primary key'), need(string(mismatch('must be a string')))),
	prop('value', need(string()))
);
let type2 = object(prop('url', need(string())));
let schema = new Schema(need(union(type1, type2)));
schema.validate({});
