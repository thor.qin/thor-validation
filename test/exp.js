/* eslint-disable @typescript-eslint/no-var-requires */
const { min, any, all, match, not, description } = require('../dist');

const rule = any(not(all(min(5), match(/ddf/)), min(10)), match(/abc/));

console.log(description(rule, 'string'));
