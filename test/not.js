/* eslint-disable @typescript-eslint/no-var-requires */
const {
	Schema,
	object,
	string,
	need,
	number,
	after,
	date,
	min,
	any,
	all,
	match,
	prop,
	not,
	union,
	between,
	range,
} = require('../dist');

let detail = object(
	prop('id', need(string(any(not(all(min(5), match(/ddf/)), min(10)), match(/abc/))))),
	prop('value', need(date(all(between('2022-2-1', '2024-2-1'), after('2020-1-1'))))),
	prop('num', need(union(number(any(range(10, 100), min(1000))), string(min(10)))))
);

try {
	let schema = new Schema(detail);
	schema.trace = 'error';
	schema.validate({ id: 'abcdddfsdfs', value: '2023-1-1', num: 150 }, true);
	console.log('OK!');
} catch (e) {
	console.error('Failed:', e.message);
}
